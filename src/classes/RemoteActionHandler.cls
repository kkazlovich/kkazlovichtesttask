/**
 * Inserts the specified exception log to the database.
 *
 */
public interface RemoteActionHandler {
	/**
	 * Inserts the specified exception log to the database.
	 *
	 * @param exceptionLog exceptionLog to be inserted to the database
	 * @return the exception log
	 */
	RemoteActionResponse getResponse(String remoteActionReq);
}