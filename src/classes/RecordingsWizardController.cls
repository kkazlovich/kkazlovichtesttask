/**
	 * Inserts the specified exception log to the database.
	 *
	 * 
	 */
public with sharing class RecordingsWizardController {

	public List<String> mockTable { get {
		return new List<String> { '' };
	} }

	/**
	 * Inserts the specified exception log to the database.
	 *
	 * @param exceptionLog exceptionLog to be inserted to the database
	 * @return the exception log
	 */
	public RecordingsWizardController() {}

	@RemoteAction
	public static RecordingsWizardArtists getArtists(String artistName) {
		RecordingsWizardArtists getArtistsResponse = new RecordingsWizardArtists();
		try {
			getArtistsResponse.accountList = [
					SELECT Id,
						   Name
					FROM Account
					WHERE Name LIKE :('%' + artistName + '%')
					ORDER BY Name
			];
		} catch (Exception e) {
			ExceptionTracker excTracker = new ExceptionTracker();
			excTracker.variablesState.put('artistName', artistName);
			excTracker.variablesState.put('getArtistsResponse', getArtistsResponse);
			getArtistsResponse.errorMessage =
					excTracker.createDebugLog(false, e).Message__c;
		}
		return getArtistsResponse;
	}

	@RemoteAction
	public static RecordingsWizardAlbum createAlbum(String albumParamsStr) {
		RecordingsWizardAlbum createAlbumResponse = new RecordingsWizardAlbum();
		try {
			createAlbumResponse.album = (Album__c) JSON.deserialize(
					albumParamsStr,
					Album__c.class
			);
			insert createAlbumResponse.album;
		} catch (Exception e) {
			ExceptionTracker excTracker = new ExceptionTracker();
			excTracker.variablesState.put('albumParamsStr', albumParamsStr);
			excTracker.variablesState.put('createAlbumResponse', createAlbumResponse);
			createAlbumResponse.errorMessage =
					excTracker.createDebugLog(false, e).Message__c;
		}
		return createAlbumResponse;
	}

	@RemoteAction
	public static RecordingsWizardSongs createSongs(String songParamListStr) {
		List<Song__c> songList;
		RecordingsWizardSongs createSongsResponse = new RecordingsWizardSongs();
		try {
			songList = (List<Song__c>) JSON.deserialize(
					songParamListStr,
					List<Song__c>.class
			);
			insert songList;
			createSongsResponse.recordsInserted = songList.size();
		} catch (Exception e) {
			ExceptionTracker excTracker = new ExceptionTracker();
			excTracker.variablesState.put('songParamListStr', songParamListStr);
			excTracker.variablesState.put('songList', songList);
			createSongsResponse.errorMessage =
					excTracker.createDebugLog(false, e).Message__c;
		}
		return createSongsResponse;
	}

	private class RecordingsWizardArtists extends RemoteActionResponse {
		List<Account> accountList;
	}

	private class RecordingsWizardAlbum extends RemoteActionResponse {
		Album__c album;
	}

	private class RecordingsWizardSongs extends RemoteActionResponse {
		Integer recordsInserted;
	}

	private class Album {
		String accountId;
		String eanIsbn;
		String format;
		String isSampler;
		String isDigital;
		String language;
		String name;
		Album(String albumParamsStr) {
			Album album = (Album) JSON.deserialize(
					albumParamsStr,
					RecordingsWizardController.Album.class
			);
			this.accountId = album.accountId;
			this.eanIsbn = album.eanIsbn;
			this.format = album.format;
			this.isSampler = album.isSampler;
			this.isDigital = album.isDigital;
			this.language = album.language;
			this.name = album.name;
		}
	}

	private class Song {
		ID accountNumber;
		Integer trackNumber;
		String eanIsbn;
		String length;
		String name;
	}
}