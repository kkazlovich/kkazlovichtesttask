/**
 * Inserts the specified exception log to the database.
 *
 * @param exceptionLog exceptionLog to be inserted to the database
 * @return the exception log
 */
public with sharing abstract class RemoteActionResponse {
	/**
	 * Inserts the specified exception log to the database.
	 *
	 * @param exceptionLog exceptionLog to be inserted to the database
	 * @return the exception log
	 */
	public String errorMessage;
	public RemoteActionResponse() {}
}