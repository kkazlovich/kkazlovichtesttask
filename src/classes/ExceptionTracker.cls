public with sharing class ExceptionTracker {

	public DebugLog__c debugLog;
	public Map<String, Object> variablesState;
	/**
	 * Inserts the specified exception log to the database.
	 *
	 * @param exceptionLog exceptionLog to be inserted to the database
	 * @return the exception log
	 */

	public ExceptionTracker() {
		variablesState = new Map<String, Object>();
	}

	public DebugLog__c createDebugLog(
			Boolean calledFromConstructor,
			Exception exc) {

		String message = exc.getMessage() + checkIfDMLException(exc);
		String stackTrace = exc.getStackTraceString();

		addMessageToPage(ApexPages.Severity.FATAL, message);

		debugLog = new DebugLog__c(
				ExceptionType__c = exc.getTypeName().replace('System.', ''),
				LineNumber__c = exc.getLineNumber(),
				Message__c = message + '\n'
					  + JSON.serializePretty(variablesState),
				Name = getClassName(stackTrace),
				OrganisationId__c = UserInfo.getOrganizationId(),
				StackTrace__c = stackTrace,
				User__c = UserInfo.getUserId()
		);
		if (!calledFromConstructor) {
			insert debugLog;
		}
		return debugLog;
	}

	/**
	 * Inserts the specified exception log to the database.
	 *
	 * @param exceptionLog exceptionLog to be inserted to the database
	 * @return the exception log
	 */
	public void addMessageToPage(
			ApexPages.Severity severity,
			String message) {

		if (ApexPages.currentPage() != null) {
			ApexPages.addMessage(
					new ApexPages.Message(
							severity,
							message
					)
			);
		}
	}

	/**
	 * Inserts the specified exception log to the database.
	 *
	 * @param exceptionLog exceptionLog to be inserted to the database
	 * @return the exception log
	 */
	private String checkIfDMLException(Exception exc) {
		String message = '';
		if (exc instanceOf DmlException) {
			DmlException dmlExc = (DmlException) exc;
			List<String> messageList = new List<String>();
			Integer numberOfRows = dmlExc.getNumDml();
			for (Integer i = 0; i < numberOfRows; i++) {
				Integer fieldIndex = dmlExc.getDmlIndex(i);
				messageList.addAll(dmlExc.getDmlFieldNames(fieldIndex));
				messageList.add(dmlExc.getDmlId(fieldIndex));
				messageList.add(dmlExc.getDmlMessage(fieldIndex));
				messageList.add('' + dmlExc.getDmlStatusCode(fieldIndex));
				messageList.add('' + dmlExc.getDmlType(fieldIndex));
			}
			if (!messageList.isEmpty()) {
				message = '\n' + String.join(messageList, '\n');
			}
		}
		return message;
	}

	/**
	 * Inserts the specified exception log to the database.
	 *
	 * @param exceptionLog exceptionLog to be inserted to the database
	 * @return the exception log
	 */
	public String getClassName(String stackTrace) {
		String className;
		if (stackTrace != null &&
				stackTrace != '' &&
				stackTrace.contains(':') &&
				stackTrace.contains('.')) {
			List<String> semicolonSplit = stackTrace.split(':');
			if (!semicolonSplit.isEmpty()) {
				List<String> dotSplit = semicolonSplit.get(0).split('\\.');
				if (dotSplit.size() > 1) {
					className = dotSplit.get(1);
				}
			}
		}
		return className;
	}
}